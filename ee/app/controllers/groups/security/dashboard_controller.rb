# frozen_string_literal: true
class Groups::Security::DashboardController < Groups::Security::ApplicationController
  layout 'group'
end
